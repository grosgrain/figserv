const http = require('http')
const fs = require('fs')
var config = JSON.parse(fs.readFileSync('config', 'utf8'))

var services = []

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('localservices.txt')
})
lineReader.on('line', function (line) {
  services.push(line)
})

const server = http.createServer((req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain')
  var retstr = ''
  for (var i = 0; i < 30; i++) {
    retstr += services[i] + '\n'
  }
  res.end(retstr)
  console.log("recvd request")
})

server.listen(config['port'], config['hostname'], () => {
  console.log(`Server running at http://${config['hostname']}:${config['port']}/`)
})

